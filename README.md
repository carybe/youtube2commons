# Scripts to download, transcode and upload videos from Youtube to Wikimedia Commons:

## Definitions
In this tutorial, we assume that all commands will start from the root project directory: `/var/tmp/youtube2commons/`

## Prerequisites:

## General use dependencies
For this tutorial to work, on Debian-based system, one may need to install the following dependencies:
```bash
sudo apt install -y git python3 python3-virtualenv make build-essential parallel perl
```

### Donwload tools
To download the videos we use [youtube-dl](https://github.com/ytdl-org/youtube-dl/), even though its repository has been temporaly taken down...
To setup the _virtualenv_ for using _youtube-dl_ one may do as follows:

```bash
virtualenv youtube-dl -p python3
source youtube-dl/bin/activate
pip install youtube-dl
deactivate
```

### Transcode tools
To transcode the videos we use [ffmpeg](https://github.com/FFmpeg/FFmpeg) with _vpx_ encoders to transcode _mp4_(usually _h264_ or _h265_) to _webm_ (_vp9_).
If your system doesn't have _vpx_ encoders, you may need to install them and, maybe, recompile ffmpeg with this functionality; to do so, use the following commands in a Debian-based system:

```bash
sudo apt install -y vpx-tools libvpx-dev ffmpeg pkg-config
```

If you need to recompile your ffmpeg:
```bash
git clone https://github.com/FFmpeg/ffmpeg
cd ffmpeg
./configure --enable-libvpx
# your nasm version may be outdated, so you need to update it (maybe from backports) or have a "crippled build", disabling x86asm:
#./configure --disable-x86asm   --enable-libvpx
make -j
sudo make install
cd ..
```

### Upload tools
To get the metadata from the files, we will need exiftools:
```bash
sudo apt install -y exiftools
```

And to upload the files to Commons, we will use the upload function from [pywikibot](https://www.mediawiki.org/wiki/Manual:Pywikibot).
To setup a _virtualenv_ and configure you user for using _pywikibot_, do as follows:

```bash
git clone --recursive --branch stable https://gerrit.wikimedia.org/r/pywikibot/core.git pywikibot
cd pywikibot
virtualenv env --prompt='(pywikibot) ' -p python3
source env/bin/activate
pip install -r requirements.txt
# For Wikimedia Commons use:
## family = commons
## language = commons
## user = <your user on commons>
./pwb.py grenerate_user_files
deactivate
cd ..
```
You may want to setup some throttle so that you don't crash wikimedia servers; for such edit `user-config.py` file.

## DOWNLOAD
To download, we use `youtube-dl`, downloading the best quality and extracting all information possible:

```bash
# Activate youtube-dl virtualenv
source youtube-dl/bin/activate

mkdir -p download
cd download
youtube-dl --format best \
           --audio-quality 0 \
           --add-metadata \
           --ffmpeg-location /var/tmp/youtube2commons/ffmpeg/ffmpeg \
           --all-subs \
           --continue \
           --no-overwrites \
           --ignore-errors \
           --write-description \
           --write-info-json \
           --write-annotations \
           --write-all-thumbnails \
           --verbose \
           --no-check-certificate \
           https://www.youtube.com/channel/UCVBqgpuXwc0UVAUEGh41q3Q &> youtube-dl.log &
# To make sure that the process doesn't die with the session (in case you are remote)
disown -a
deactivate
cd ..
```

## TRANSCODE
After the download finishes (you may follow its progress by watching the download log file `tail -f download/youtube-dl.log`), we need to convert every `mp4` file to `webm` and for such, we can use GNU `parallel` tool to speed up things:

```bash
mkdir -p webm
find download -type f -name '*.mp4' | parallel -j $(nproc) \
                                               --verbose \
                                               /var/tmp/youtube2commons/ffmpeg/ffmpeg -nostdin \
                                                      -hide_banner \
                                                      -nostats \
                                                      -loglevel panic \
                                                      -i {} \
                                                      -c:v libvpx-vp9 \
                                                      -threads 1 \
                                                      -b:v 0 \
                                                      -crf 30 \
                                                      -pass 1 \
                                                      -row-mt 1 \
                                                      -an \
                                                      -f webm \
                                                      -y "/dev/null" '&&' \
                                               /var/tmp/youtube2commons/ffmpeg/ffmpeg -i {} \
                                                      -c:v libvpx-vp9 \
                                                      -threads 1 \
                                                      -b:v 0 \
                                                      -crf 30 \
                                                      -pass 2 \
                                                      -row-mt 1 \
                                                      -c:a libopus webm/{/.}.webm \
                                               &> mp42wbm.log &
# To make sure that the process doesn't die with the session (in case you are remote)
disown -a
```

## UPLOAD
After the transcode finishes (you may follow its progress by watching the trancode log file `tail -f mp42webm.log`), you should probably save your progress to some safe and shared storage (maybe some cloud storage, like Google Drive).
Then we are finally set to upload the files to Commons, and, for such, a well formated description is necessary.
To **generate a description**, you may use the following script:
```bash
#!/bin/bash

# desc_gen.sh

find webm -name "*.webm" | while read FILE
do

    echo "${FILE}"

    BASE_FILE=$(echo "${FILE}" | sed -E 's/\..{2,4}$//')
    DESCRIPTION_FILE="${BASE_FILE}.nfo"

    DESCRIPTION=$($exiftool -a -s -s -s "${FILE}" | grep -A1 DESCRIPTION | tail -n1)
    DATE=$(date -d $($exiftool -a -s -s -s "${FILE}" | grep -A1 DATE | tail -n1) +%Y-%m-%d)
    TITLE=$($exiftool -title -s -s -s "${FILE}")
    URL=${FILE:${#FILE}-16:11}

    # NeuroMat file description template:
cat << EOF > "${DESCRIPTION_FILE}"
=={{int:filedesc}}==
{{Information
	|description={{pt|1=${DESCRIPTION}}}
	|date=${DATE}
	|source={{From YouTube|1=${URL}|2=${TITLE}}}
	|author=[https://www.youtube.com/user/neuromathematics Comunicação NeuroMat]
	|permission=
	|other_versions=
	|other_fields=
}}

=={{int:license-header}}==
{{NeuroMat-license}}
{{LicenseReview}}

[[Category:NeuroMat videos]]

EOF

done
```

Now we have at `webm/` both `.webm` and `.nfo` files with video and description respectively and we are all set to upload those files to Commons.
To do so, one can use the following script automating the whole process:

```bash
#!/bin/bash

# upload_script.sh

source /var/tmp/youtube2commons/pywikibot/env/bin/activate
pwb=/var/tmp/youtube2commons/pywikibot/pwb.py

find webm -name "*.webm" | while read FILE
do

    FILENAME=$(basename "${FILE}")

    # Some files may have in it's names characters that are not compatible with Wikimedia Commons, we replace such characters with '_'
    SANITIZED_FILENAME=$(echo "${FILENAME}" | perl -pe 's/[\x1f\x23\x3c\x3e\x5b\x5d\x7b\x7c\x7d\x7f]/_/g')

    echo "${FILE}"

    $pwb upload -always \
                -ignorewarn \
                "${FILE}" \
                -descfile:"${FILE%%.webm}.nfo" \
                -filename:"${SANITIZED_FILENAME}" \
                -chunked:16m

# Additional throttling to make sure we are not lagging Wikimedia servers too much
echo waiting a minute...
sleep 60
done
```

It is **VERY** probable that your account will be blocked and thats why you must keep track of every command/script progress redirecting its outputs to a file (e.g. `./upload_script.sh &> upload.log &`), separating, after every trial, the files that were sucessfully uploaded from those that weren't.

Some large files might not be uploaded due to connection instability and server overload; if that's the case, you must upload it manually using the browser interface.

After all these steps, you must have uploaded your whole library to Commons; now you must go there and manually check every file uploaded to see if everything is correct.
