#!/bin/bash

exiftool=/var/tmp/yt/exiftool/exiftool

FILE="${1}"
BASE_FILE=$(echo "${FILE}" | sed -E 's/\..{2,4}$//')
DESCRIPTION_FILE="${BASE_FILE}.nfo"

echo "${1}"

DESCRIPTION=$($exiftool -a -s -s -s "${FILE}" | grep -A1 DESCRIPTION | tail -n1)

DATE=$(date -d $($exiftool -a -s -s -s "${FILE}" | grep -A1 DATE | tail -n1) +%Y-%m-%d)

TITLE=$($exiftool -title -s -s -s "${FILE}")

URL=${FILE:${#FILE}-16:11}

cat << EOF > "${DESCRIPTION_FILE}"
=={{int:filedesc}}==
{{Information
	|description={{pt|1=${DESCRIPTION}}}
	|date=${DATE}
	|source={{From YouTube|1=${URL}|2=${TITLE}}}
	|author=[https://www.youtube.com/user/neuromathematics Comunicação NeuroMat]
	|permission=
	|other_versions=
	|other_fields=
}}

=={{int:license-header}}==
{{NeuroMat-license}}
{{LicenseReview}}

[[Category:NeuroMat videos]]

EOF
