#!/bin/bash

pwb=/var/tmp/yt/pywikibot/pwb.py

source /var/tmp/youtube2commons/pywikibot/env/bin/activate

FILE=${1}
FILENAME=$(basename "${FILE}")

SANITIZED_FILENAME=$(echo "${FILENAME}" | perl -pe 's/[\x1f\x23\x3c\x3e\x5b\x5d\x7b\x7c\x7d\x7f]/_/g')

#$pwb

echo "${FILE}"

$pwb upload -always \
            -ignorewarn \
            "${FILE}" \
            -descfile:"${FILE%%.webm}.nfo" \
            -filename:"${SANITIZED_FILENAME}" \
            -chunked:32m

echo waiting a minute...
sleep 60
